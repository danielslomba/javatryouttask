package com.example.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Data
@Component
@ConfigurationProperties(prefix = "commission.rules")
public class CommissionRulesProperties {
    private final BigDecimal ruleOneDefaultRuleOneCommissionFactor = BigDecimal.valueOf(0.005);
    private final BigDecimal ruleOneMinimumCommission = BigDecimal.valueOf(0.05);
}
