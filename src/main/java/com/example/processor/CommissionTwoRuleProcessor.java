package com.example.processor;

import com.example.data.CommissionApplied;
import com.example.data.CommissionData;
import com.example.data.CommissionResult;

import java.math.BigDecimal;

public class CommissionTwoRuleProcessor extends CommissionRuleProcessor {

    private final BigDecimal ruleTwoCommission = BigDecimal.valueOf(0.05);

    public CommissionTwoRuleProcessor(CommissionRuleProcessor nextCommissionProcessor) {
        super(nextCommissionProcessor);
    }

    @Override
    public CommissionResult handleCommission(CommissionData data, CommissionResult currentResult) {
        boolean isClientId42 = data.clientId() == 42;
        CommissionResult newCommissionResult = currentResult;
        if (isClientId42) {
            if (ruleTwoCommission.compareTo(currentResult.commissionValue()) < 0) {
                newCommissionResult = new CommissionResult(ruleTwoCommission, CommissionApplied.RULE2);
            }
        }
        if (nextCommissionProcessor != null) {
            return this.nextCommissionProcessor.handleCommission(data, newCommissionResult);
        }
        return newCommissionResult;
    }
}
