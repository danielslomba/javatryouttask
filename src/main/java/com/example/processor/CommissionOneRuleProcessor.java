package com.example.processor;

import com.example.data.CommissionApplied;
import com.example.data.CommissionData;
import com.example.data.CommissionResult;

import java.math.BigDecimal;

public class CommissionOneRuleProcessor extends CommissionRuleProcessor {

    private final BigDecimal defaultCommissionFactor = BigDecimal.valueOf(0.005);
    private final BigDecimal minimumCommission = BigDecimal.valueOf(0.05);

    public CommissionOneRuleProcessor(CommissionRuleProcessor nextCommissionProcessor) {
        super(nextCommissionProcessor);
    }

    @Override
    public CommissionResult handleCommission(CommissionData data, CommissionResult commissionResult) {
        BigDecimal defaultCommission = applyMinimumThresholdCommission(
                data.thisTransactionAmountInEUR().multiply(defaultCommissionFactor), minimumCommission);

        CommissionResult currentResult = new CommissionResult(defaultCommission, CommissionApplied.RULE1);

        if (nextCommissionProcessor != null) {
            return this.nextCommissionProcessor.handleCommission(data, currentResult);
        }
        return currentResult;

    }

    private BigDecimal applyMinimumThresholdCommission(BigDecimal defaultCommission, BigDecimal
            minimumCommission) {
        if (defaultCommission.compareTo(minimumCommission) < 0)
            defaultCommission = minimumCommission;
        return defaultCommission;
    }
}
