package com.example.processor;

import com.example.data.CommissionData;
import com.example.data.CommissionResult;

abstract class CommissionRuleProcessor {
    protected CommissionRuleProcessor nextCommissionProcessor;

    public CommissionRuleProcessor(CommissionRuleProcessor nextCommissionProcessor) {
        this.nextCommissionProcessor = nextCommissionProcessor;
    }

    public abstract CommissionResult handleCommission(CommissionData commissionData, CommissionResult currentResult);
}
