package com.example.processor;

import com.example.data.CommissionApplied;
import com.example.data.CommissionData;
import com.example.data.CommissionResult;
import com.example.repository.CommisionsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

@Slf4j
public class CommissionThreeRuleProcessor extends CommissionRuleProcessor {

    private String EUR = "EUR";
    private final BigDecimal ruleThreeCommission = BigDecimal.valueOf(0.03);

    @Autowired
    private CommisionsRepository commissionsRepository;

    public CommissionThreeRuleProcessor(CommissionRuleProcessor nextCommissionProcessor) {
        super(nextCommissionProcessor);
    }

    @Override
    public CommissionResult handleCommission(CommissionData data, CommissionResult currentResult) {
        boolean monthlyTurnoverReached = false;
        CommissionResult newCommissionResult = currentResult;
        if (data.alreadyProcessedMonthlyTurnover() != null) {
            // if equal or greater than 1000 EUR
            // pass 1000 to a properties file
            monthlyTurnoverReached = data.alreadyProcessedMonthlyTurnover().compareTo(BigDecimal.valueOf(1000)) >= 0;
        }
        log.debug("Client clientId {} processed monthly turnover was {} {}", data.clientId(), data.alreadyProcessedMonthlyTurnover(), EUR);
        if (monthlyTurnoverReached) {
            if (ruleThreeCommission.compareTo(currentResult.commissionValue()) < 0) {
                newCommissionResult = new CommissionResult(ruleThreeCommission, CommissionApplied.RULE3);
            }
        }
        if (nextCommissionProcessor != null) {
            return this.nextCommissionProcessor.handleCommission(data, newCommissionResult);
        }
        return newCommissionResult;
    }
}
