package com.example.data;

import java.math.BigDecimal;
import java.util.Date;

public record CommissionData(BigDecimal thisTransactionAmountInEUR, int clientId, Date requestedDate,
                             BigDecimal alreadyProcessedMonthlyTurnover) {
}