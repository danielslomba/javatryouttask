package com.example.data;

import java.math.BigDecimal;

public record CommissionResult(BigDecimal commissionValue, CommissionApplied commissionAppliedType) {
}
