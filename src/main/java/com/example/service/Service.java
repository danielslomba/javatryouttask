package com.example.service;

import com.example.data.*;
import com.example.processor.CommissionOneRuleProcessor;
import com.example.processor.CommissionThreeRuleProcessor;
import com.example.processor.CommissionTwoRuleProcessor;
import com.example.repository.CommisionsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Slf4j
@org.springframework.stereotype.Service
public class Service {

    private static String EUR = "EUR";
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private CommisionsRepository commissionsRepository;

    @Transactional
    //TODO: if other services can access the commissions repository ate the same time
    // a @Retryable could be implemented (Then deal with Pessimistic Locking/Optimistic Locking
    public ResponseEntity<Object> addTransaction(RequestType requestBody) {

        Date requestedDate = requestBody.date();
        int clientId = requestBody.clientId();

        // Not thread safe, so better to instantiate each time
        String dateYearMonthDay = new SimpleDateFormat("yyyy-MM-dd").format(requestedDate);
        // get rates
        ResponseEntity<Object> responseEntity = restTemplate.exchange("https://api.exchangerate.host/" + dateYearMonthDay, HttpMethod.GET, HttpEntity.EMPTY, Object.class);

        if (!responseEntity.getStatusCode().equals(HttpStatus.OK) || !responseEntity.hasBody()) {
            return new ResponseEntity<>("Getting currency rates failed.", HttpStatus.INTERNAL_SERVER_ERROR);
        }

        // Get transaction amount in EUR
        BigDecimal thisTransactionAmountInEUR = getOrConvertTransactionAmountInEUR(responseEntity, requestBody, requestBody.clientId());

        // extracted from rule 3 to keep the repository call on the service.
        String requestedYearAndMonth = new SimpleDateFormat("yyyy-MM").format(requestedDate);
        BigDecimal alreadyProcessedMonthlyTurnover = commissionsRepository.getClientSumOfTurnoverPerMonth(clientId, requestedYearAndMonth);

        CommissionData data = new CommissionData(thisTransactionAmountInEUR, clientId, requestedDate, alreadyProcessedMonthlyTurnover);

        CommissionOneRuleProcessor lowestCommissionRuleChain = new CommissionOneRuleProcessor(new CommissionTwoRuleProcessor(new CommissionThreeRuleProcessor(null)));
        CommissionResult commissionResult = lowestCommissionRuleChain.handleCommission(data, null);
        log.info("Client client_id {} applied commision {} = {} EUR on {}", clientId, commissionResult.commissionAppliedType(), commissionResult.commissionValue(), dateYearMonthDay);

        // get current date CommissionType
        CommissionType commissionType = commissionsRepository.findByIdAndDate(clientId, requestedDate);

        if (commissionType == null) {
            commissionType = new CommissionType(0, clientId, requestBody.date(), thisTransactionAmountInEUR);
        } else {
            // get processed daily turnover add this transaction amount and save to db
            commissionType.setAmount(commissionType.getAmount().add(thisTransactionAmountInEUR));
        }
        commissionsRepository.save(commissionType);

        return new ResponseEntity<>(new ResponseType(commissionResult.commissionValue().setScale(2, RoundingMode.CEILING), EUR), HttpStatus.CREATED);
    }

    // pass only needed values. Not complex ones
    private BigDecimal getOrConvertTransactionAmountInEUR(ResponseEntity<Object> responseEntity, RequestType request, int clientId) {
        String thisTransactionCurrency = request.currency();
        BigDecimal thisTransactionAmount = request.amount();

        if (thisTransactionCurrency.equals(EUR)) {
            return thisTransactionAmount;
        }

        BigDecimal currencyRate = getCurrencyRate(responseEntity, thisTransactionCurrency);

        // convert thisTransactionAmount to EUR with 2 DECIMAL
        BigDecimal thisTransactionAmountInEUR = thisTransactionAmount.divide(currencyRate, 2, RoundingMode.CEILING);
        log.info("Client clientId {} converted {} {} to {} {}", clientId, thisTransactionAmount, thisTransactionCurrency, thisTransactionAmountInEUR, EUR);
        return thisTransactionAmountInEUR;
    }

    private BigDecimal getCurrencyRate(ResponseEntity<Object> responseEntity, String thisTransactionCurrency) {
        BigDecimal currencyRate;
        final Map<?, ?> rates = (Map) ((Map) responseEntity.getBody()).get("rates");
        try {
            Double currencyRateDouble = (Double) rates.get(thisTransactionCurrency);
            currencyRate = BigDecimal.valueOf(currencyRateDouble.doubleValue());
        } catch (Exception e) {
            throw new IllegalArgumentException("Error while converting currency rate. Rates available: " + rates + ".Current transaction currency: " + thisTransactionCurrency + ". " + e);
        }
        return currencyRate;
    }
}

