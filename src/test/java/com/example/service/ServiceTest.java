package com.example.service;

import com.example.data.CommissionType;
import com.example.data.RequestType;
import com.example.data.ResponseType;
import com.example.repository.CommisionsRepository;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@WebMvcTest(Service.class)
class ServiceTest {

	protected static ObjectMapper objectMapper = new ObjectMapper().enable(JsonParser.Feature.ALLOW_COMMENTS).setSerializationInclusion(JsonInclude.Include.NON_NULL);

	@MockBean
	private RestTemplate restTemplate;

	@Autowired
	private Service service;

	@MockBean
	private CommisionsRepository commissionsRepository;

	static Date requestedDate;
	static String dateYearMonth;
	
	@BeforeAll
	static void beforeAll() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2021);
		cal.set(Calendar.MONTH, Calendar.JANUARY);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		requestedDate = cal.getTime();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
		dateYearMonth = simpleDateFormat.format(requestedDate);
	}

	@ParameterizedTest
	@CsvSource({
			"123, 500, 50, 100, 0.11, PLN",
			"42, 500, 50, 100, 0.05, PLN",
			"42, 1000, 0, 100, 0.03, PLN",
			"42, 1000, 0, 30, 0.03, PLN",
			"123, 1001, 50, 100, 0.03, PLN"
	})
	void addTransactionRule1CommissionApply_UnitTest(int clientId, int consumedTurnoverPerMonth, int consumedTurnoverForCurrentDay,
													 int currentTurnover, double expectedCommission, String currency) throws IOException {

		Mockito.when(restTemplate.exchange(Mockito.anyString(), Mockito.any(HttpMethod.class), Mockito.any(HttpEntity.class), Mockito.any(Class.class))).thenReturn(new ResponseEntity<Object>(readJson("/exchange_rate/response_body.json"), HttpStatus.OK));
		Mockito.when(commissionsRepository.getClientSumOfTurnoverPerMonth(Mockito.anyInt(), Mockito.anyString())).thenReturn(BigDecimal.valueOf(consumedTurnoverPerMonth));
		Mockito.when(commissionsRepository.findByIdAndDate(Mockito.anyInt(), Mockito.any(Date.class))).thenReturn(new CommissionType(0, clientId, requestedDate, BigDecimal.valueOf(consumedTurnoverForCurrentDay)));

		ResponseEntity<?> responseEntity = service.addTransaction(new RequestType(requestedDate, BigDecimal.valueOf(currentTurnover), currency, clientId));

		Assertions.assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
		Assertions.assertEquals(BigDecimal.valueOf(expectedCommission), ((ResponseType) responseEntity.getBody()).amount());
	}

	@Test
	void addTransactionRuleCommissionApply_InternalServerError_UnitTest() throws IOException {
		Mockito.when(restTemplate.exchange(Mockito.anyString(), Mockito.any(HttpMethod.class), Mockito.any(HttpEntity.class), Mockito.any(Class.class))).thenReturn(new ResponseEntity<Object>(readJson("/exchange_rate/response_body.json"), HttpStatus.INTERNAL_SERVER_ERROR));
		ResponseEntity<?> responseEntity = service.addTransaction(new RequestType(requestedDate, BigDecimal.valueOf(1), "PLN", 111));
		Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
	}

	public static Map<?, ?> readJson(String path) throws IOException {
		try {
			InputStream is = ServiceTest.class.getResourceAsStream(path);
			return objectMapper.readValue(is, LinkedHashMap.class);
		} catch (Throwable var2) {
			throw var2;
		}
	}

}
