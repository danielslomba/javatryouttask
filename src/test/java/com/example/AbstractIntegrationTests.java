package com.example;

import com.example.data.RequestType;
import com.example.data.ResponseType;
import org.junit.jupiter.api.AfterAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;

import java.math.BigDecimal;
import java.util.Date;

public class AbstractIntegrationTests {

    static String AMOUNT_CURRENCY = "PLN";

    @Container
    static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:11.1");

    @DynamicPropertySource
    static void registerMySQLProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgreSQLContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgreSQLContainer::getUsername);
        registry.add("spring.datasource.password", postgreSQLContainer::getPassword);
    }

    @Autowired
    TestRestTemplate testRestTemplate;

    @AfterAll
    static void stopDbContainer() {
        postgreSQLContainer.stop();
    }

    public ResponseEntity<ResponseType> addTransaction(String url, Date date, BigDecimal amount, String amountCurrency, int clientId) {
        RequestType requestTypeRule1Apply1stClient = new RequestType(date, amount, amountCurrency, clientId);
        HttpEntity<RequestType> httpEntityRule1Apply1stClient = new HttpEntity<>(requestTypeRule1Apply1stClient);
        return testRestTemplate.exchange(url, HttpMethod.POST, httpEntityRule1Apply1stClient, ResponseType.class);
    }
}
